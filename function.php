<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки


function isAuthor() 
{         // Проверяю, передан ли автор книги методом пост
	return  $_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['author']); 
}; 

function isISBN() 
{       // Проверяю, передан ли ISBN методом пост
return $_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['isbn']);
};

function isName() 
{         // Проверяю, передано ли название книги методом пост
	return  $_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['name']); 
}; 