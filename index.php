<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
require_once('function.php');

$host = 'localhost';    // localhost 
$user = 'karpayev';
$password = 'neto1052'; 
$database = 'global'; 
$dbport = 3306; 

$pdo = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $password, [
		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
	]);

// $author = (string) ( $_POST['author'] ?? "" ); 
// $isbn = (string) ( $_POST['isbn'] ?? "" ); 
// $name = (string) ( $_POST['name'] ?? "" ); 
// $year = (integer) ( $_POST['year'] ?? "" ); 

$author = isAuthor() ? (string)$_POST['author'] : ''; 
$isbn = isISBN() ? (string)$_POST['isbn'] : ''; 
$name = isName() ? (string)$_POST['name'] : ''; 

$statement = $pdo->prepare("SELECT * FROM books WHERE author LIKE ? AND name LIKE ? AND isbn LIKE ?;");    // prepare - подготовить запрос
$statement->execute( [              // execute - выполнять запрос
	"%{$author}%", 
	"%{$name}%", 
	"%{$isbn}%", 	
] );     
$rows = $statement->fetchAll(PDO::FETCH_ASSOC); 
?>

<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>books</title>
<style>
body {
 font-family: sans-serif;
 font-size: 15px;
}
table {
border: 1px solid black; 
padding: 4px;
}
td {
border: 1px solid black; 
padding: 4px;
}
</style>
</head>
<body>
<h1>Книги</h1>
<form method="POST">
    <input type="text" name="isbn" placeholder="ISBN" value="<?php echo $isbn; ?>" />
    <input type="text" name="name" placeholder="Название книги" value="<?php echo $name; ?>" />
    <input type="text" name="author" placeholder="Автор книги" value="<?php echo $author; ?>" />
    <input type="submit" value="Найти книгу" />
</form>
<br /><br />

<table>

	<tr>
		<th>id</th>
        <th>Название</th>
        <th>Автор</th>
        <th>Год выпуска</th>
        <th>ISBN</th>
        <th>Жанр</th>
	</tr>

<?php  foreach ( $rows as $row )  :  ?> 
	<tr>
	<?php  foreach ( $row as $string )  :  ?> 
		<td><?php  echo $string;  ?></td>
	<?php  endforeach;  ?>
	</tr>
<?php  endforeach;  ?>

</table>
</body>
</html>